﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using WebApplication1.Facade;
using WebApplication1.Models;
using WebApplication1.ServiceLocator;
using WebApplication1.CommonServiceFacade;
using Microsoft.Extensions.Caching.Memory;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public SmtpConfig SmtpConfig { get; }
        private CacheManager _cacheManager;
        public HomeController(IHttpClientFactory httpClientFactory, IOptions<SmtpConfig> smtpConfig,IMemoryCache memoryCache)
        {
            _httpClientFactory = httpClientFactory;
            SmtpConfig = smtpConfig.Value;
            _cacheManager=new CacheManager(memoryCache);

        }

        public IActionResult Index()
        {
            _cacheManager.Remove(CacheManager.CacheCategory.Subscription, "Amarjeet_Key");
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Added to cache : " + DateTime.Now.ToString();

            _cacheManager.Insert(CacheManager.CacheCategory.Subscription, "Amarjeet_Key",DateTime.Now.ToString(), 20);
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] =_cacheManager.GetValue(CacheManager.CacheCategory.Subscription, "Amarjeet_Key");
            
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<PartialViewResult> Test()
        {
            ViewData["Message"] = "Test Ajax";
            //ServiceLocatorTool serviceLocator = new ServiceLocatorTool();
            // SubscriptionFacade facade = new SubscriptionFacade(serviceLocator.ObjServiceLocator);
            var PortalId = Request.Query["PortalId"];
            SubscriptionFacade facade = new SubscriptionFacade(_httpClientFactory);
            //var client = _httpClientFactory.CreateClient("github");
            //var response = await client.GetAsync($"/bc/secure/subscription?subscriptionId=87341127");  //facade.SubscriptionFacade1();

            //var stringResult = await response.Content.ReadAsStringAsync();
            //var subs = JsonConvert.DeserializeObject<List<Subscription>>(stringResult);
            var subscriptionId="8731127";
            
            List<Subscription> subs = _cacheManager.Get<List<Subscription>>(CacheManager.CacheCategory.Subscription, subscriptionId + '_' + PortalId, () => facade.SearchSubscriptionsAsync(subscriptionId), 20);
            
            
            //var subs = facade.SearchSubscriptionsAsync("8731127");

            return PartialView( new HomePageViewModel { Test1 = "Acc No.:-"+subs[0].parentAccountNo.ToString(), Test2 = "Test 2" });
        }

        public async Task<PartialViewResult> RemoveCache()
        {
            var PortalId = Request.Query["PortalId"];
            var subscriptionId="8731127";
            _cacheManager.Remove(CacheManager.CacheCategory.Subscription, subscriptionId + '_' + PortalId);
            return PartialView( new HomePageViewModel { Test1 = "Cache Removed" , Test2 = "Test 2" });
        }

        public string TestData()
        {
            ViewData["Message"] = "Test Ajax";

            return JsonConvert.SerializeObject( new HomePageViewModel { Test1 = HttpContext.Request.Host.ToString()+HttpContext.Request.Path, Test2 = "Test 2" });
        }
    }
}
