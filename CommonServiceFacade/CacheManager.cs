﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace WebApplication1.CommonServiceFacade
{
    public  class CacheManager :Controller
    {
        private  IMemoryCache _cache;
         
      //Initialize IMemoryCache
       public CacheManager(IMemoryCache memoryCache)
        {
        _cache = memoryCache;
       
        }
        public enum CacheCategory
        {
            Default = 0,
            GuideAndArticleSelector = 1,
            AddressSearch = 2,
            Subscription = 3,
            RetailCustomer = 4,
            NoteDetails = 5,
            SpecDetails = 6,
            ProvisionalChannelList = 7,
            Pulse = 8,
            TimelineTemplates = 9,
            LineStateResult=10,
            LineCheckDiagnoseLid=11,
            CustomerOverviewDetails = 12,
            LineDiagnoseResult=13,
            DLMStatus=14,
            LineStateParameter=15,
        }
        

        /// <summary>
        /// Retrieves the Cache with the given Key and Category. If Not found then inserts the Data using Source Function into the cache.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="category">User Defined Category which will be attached to Cache Name.</param>
        /// <param name="key">Key name for Cache</param>
        /// <param name="dataGenerator">Source of data function</param>
        /// <param name="durationInMinutes">Set to Zero for No Absolute Expiration else Non-Zero for Absolute Expiration</param>
        /// <returns>Returns Cached Data</returns>
        public  T Get<T>(CacheCategory category, string key, Func<T> dataGenerator, int durationInMinutes)
        {
            
            _cache.TryGetValue(GenerateKey(category, key),out T value);
           

            if (value == null)
            {
                Console.WriteLine("************  Cache NOT FOUND ********\n");
                value = dataGenerator();
                if (value is IList && (value as IList).Count == 0)
                {
                    return value;//don't cache empty objects
                }
                else
                {
                    Insert(category, key, value, durationInMinutes);
                }
            }else{
                 Console.WriteLine("***** Cache Found as below    ******\n");
                Console.WriteLine(value);
            }

            return value;
        }

        public  void Insert<T>(CacheCategory category,  string key, T value, int durationInMinutes)
        {
             _cache.TryGetValue(GenerateKey(category, key),out T CacheValue);
             if(CacheValue==null)
             {
            if (durationInMinutes == 0)
            {
                _cache.Set(GenerateKey(category, key), value, DateTimeOffset.MaxValue);
               
            }
            else
            {
                _cache.Set(GenerateKey(category, key), value, DateTime.Now.AddMinutes(durationInMinutes));
                Console.WriteLine("****************** Cache Set with : Key= " +GenerateKey(category,key)+"\n");
            }
             }
        }
        /// <summary>
        /// </summary>
        /// <Project>Service Dashboard BLR 71012</Project>
        /// <typeparam name="T"></typeparam>
        /// <param name="category">User Defined Category which will be attached to Cache Name.</param>
        /// <param name="key">Key name for Cache</param>
        /// <param name="dataGenerator">Source of data function</param>
        /// <param name="durationInMinutes">Set to Zero for No Absolute Expiration else Non-Zero for Absolute Expiration</param>
        /// <returns>Returns Cached Data</returns>
        public  void Update<T>(CacheCategory category, string key, Func<T> dataGenerator, int durationInMinutes)
        {
            T value = dataGenerator();
            if (durationInMinutes == 0)
            {
                 _cache.Set(GenerateKey(category, key), value,DateTimeOffset.MaxValue);
            }
            else
            {
            _cache.Set(GenerateKey(category, key), value,DateTime.Now.AddMinutes(durationInMinutes));
        }
        }
        private  string GenerateKey(CacheCategory category, string key)
        {
            return category.ToString() + "_" + key;
        }

        public  void Remove(CacheCategory category, string key)
        {            
            _cache.Remove(GenerateKey(category, key));
            Console.WriteLine("****Cache with key "+ GenerateKey(category,key) +" REMOVED ***************\n");
        }

        public  string GetValue(CacheCategory category, string key)
        {            
            _cache.TryGetValue(GenerateKey(category, key),out string value);
            if(value!=null)
            return value;
            else
            return "Cache Not Found";
        }
    }
}
